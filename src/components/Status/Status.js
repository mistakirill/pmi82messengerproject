import React from 'react';

const Status = ({className, status, isActive}) => {
  return (
      <section className={`${className} ${isActive}`}>
        <section className={status}>
          <svg>
            <g>
                <circle cx="5" cy="5" r="5" stroke-width="4" fill="yellow"> </circle>
            </g>
          </svg>
        </section>
      </section>
  )
};
export default Status;
