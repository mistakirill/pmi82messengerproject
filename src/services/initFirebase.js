import * as firebase from 'firebase';

const app = firebase.initializeApp({
  apiKey: "AIzaSyCnBh-7xiDsu_s2-bJ6BpaDySa5rnd8vmU",
  authDomain: "web-project-react.firebaseapp.com",
  databaseURL: "https://web-project-react-default-rtdb.firebaseio.com",
  projectId: "web-project-react",
  storageBucket: "web-project-react.appspot.com",
  messagingSenderId: "976210979192",
  appId: "1:976210979192:web:ec262fe4879533d932c08d"
});

export default app;
